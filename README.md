# Chronos

This was created by Lucas TESSON (lucas.tesson@etu.univ-lyon1.fr) for the supervised project "MandelBulb: fractales 3D". Run on Linux systems.

## Compilation

You can compile this project using `g++ -o main.exe -O3 -pthread -std=c++11 -Wall main.cpp`.

## Running

You can run this project using `./main.exe`.
Parameters can be given when running the program :
- `-help` will give you help.
- `-threads=[VALUE]` to give a number of threads to work on. Default is the maximum amount of threads on this calculator.
- `-batch=[VALUE]` to give a number of batch to work on. Default is 30.
- `-outputFile=[NAME]` to give the name of the file to save data in. Notice that you can give a path and an extension, but data will be exported as csv. Default is output.csv in the same directory.
- `-fullStats=[BOOL]` to tell if you want to export all the chronos or if you only want averages. Default is true.
