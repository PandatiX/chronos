#include <iostream>
#include <string>
#include <thread>
#include <fstream>

//Window size for progress bar
#include <sys/ioctl.h>
#include <unistd.h>

void err(const char*e) {
	std::cerr << "\033[1;31mERROR: " << e << "\033[0m" << std::endl;
}

class MandelBrot {
private:
	//16K resolution, 16:9
	int WIDTH = 1920 * 8;
	int HEIGHT = 1080 * 8;

	int zoom = 100;
	int iteration_max = 256;
	float x1 = -2.1f;
	float x2 = 0.6f;
	float y1 = -1.2f;
	float y2 = 1.2f;
	float bias_x = 0.0f;
	float bias_y = 0.0f;
	
	float image_x = (float)WIDTH;
	float image_y = (float)HEIGHT;

	void computeBounds(int indexThread, int nbThreads, float& start_x, float& stop_x) {

		int width = (int)(image_x / nbThreads);
		start_x = (indexThread)*width;

		if (indexThread < nbThreads - 1) {
			stop_x = (indexThread+1)*width - 1;
		}
		else {
			stop_x = image_x;
		}

	}

	void calculate(int indexThread, int nbThreads) {

		float start_x, stop_x;
		computeBounds(indexThread, nbThreads, start_x, stop_x);

		for (float x = start_x; x < stop_x; ++x) {
		    for (float y = 0; y < image_y; ++y) {

			float c_r = x / (float) zoom + x1 + bias_x;
			float c_i = y / (float) zoom + y1 + bias_y;

			float z_r = 0;
			float z_i = 0;

			int i = 0;

			do {

			    float tmp = z_r;

			    z_r = z_r * z_r - z_i * z_i + c_r;
			    z_i = 2 * z_i * tmp + c_i;
			    i++;

			} while (z_r * z_r + z_i * z_i < 4 && i < iteration_max);

		    }

		    computeBounds(indexThread, nbThreads, start_x, stop_x);

		}

	}

public:
	MandelBrot() = default;
	~MandelBrot() = default;
	void setupThreads(unsigned int nbThreads, unsigned int& resultChrono){

		nbThreads = (nbThreads<1)?1:(nbThreads>std::thread::hardware_concurrency())?std::thread::hardware_concurrency():nbThreads;

		std::thread **threads = new std::thread*[nbThreads];

		std::chrono::time_point<std::chrono::system_clock> start, end;
    		start = std::chrono::system_clock::now();

		for (unsigned int i = 0 ; i < nbThreads ; i++) {

			threads[i] = new std::thread(&MandelBrot::calculate, this, i, nbThreads);

		}

		for (unsigned int i = 0; i < nbThreads; i++) {

			threads[i]->join();

		}
		end = std::chrono::system_clock::now();

		for (unsigned int i = 0; i < nbThreads; i++)
			delete threads[i];
		delete threads;

		resultChrono = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();

	}

};

class Chronos {
private:
	unsigned int nbThreads;
	unsigned int pass;
	unsigned int **chrono;
	std::string outputFile;
	bool fullStats;

public:
	Chronos(unsigned int _nbThreads, unsigned int _pass, std::string _outputFile, bool _fullStats):nbThreads((_nbThreads<1)?1:(_nbThreads>std::thread::hardware_concurrency())?std::thread::hardware_concurrency():_nbThreads),pass((_pass<1)?1:_pass),outputFile(_outputFile),fullStats(_fullStats) {
		chrono = new unsigned int*[nbThreads];
		for (unsigned int i = 0; i < nbThreads; i++)
			chrono[i] = new unsigned int[pass];
	}

	void start() {
		std::cout << "\033[1m##### STARTED CHRONOS #####\033[0m" << std::endl
			<< "  Threads     = " << nbThreads << std::endl
			<< "  Batch size  = " << pass << std::endl
			<< "  Output file = " << outputFile << std::endl
			<< "  Full stats  = " << ((fullStats)?"true":"false") << std::endl;

		struct winsize size;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
		int nbDone = 1, nbTodo = nbThreads*pass, lengthHsh, percent;
		MandelBrot *mb = new MandelBrot();

		for (unsigned int nThread = 1; nThread <= nbThreads; nThread++) {
			for (unsigned int passN = 0; passN < pass; passN++, nbDone++) {
				percent = 100*nbDone/nbTodo;
				std::cout << "\r\033[30;42mProgress: [";
				if (percent < 100) {
					std::cout << " ";
					if (percent < 10)
						std::cout << " ";
				}
				std::cout << percent << "%]\033[0m [";

				lengthHsh = (int)(((size.ws_col-21)*nbDone)/nbTodo);
				for (int i = 0; i < lengthHsh; i++) std::cout << "#";
				for (int i = lengthHsh; i < size.ws_col - 21; i++) std::cout << ".";
				std::cout << "]" << std::flush;

				mb->setupThreads(nThread,chrono[nThread-1][passN]);
			}
		}
		delete mb;

		std::cout << '\r';
		for (int i = 0; i < size.ws_col-2; i++) std::cout << " ";
		std::cout << "\r\033[1m##### ENDED CHRONOS #####\033[0m" << std::endl;
	}

	void writeStats() {
		try {
			std::ofstream output;
			output.open(outputFile);
			if (fullStats) {
				output << "nbThreads,nPass,time" << std::endl;
				for (unsigned int i = 0; i < nbThreads; i++) {
					for (unsigned int y = 0; y < pass; y++) {
						output << (i+1) << "," << (y+1) << "," << chrono[i][y] << std::endl;
					}
				}
			} else {
				output << "nbThreads,timeMoy" << std::endl;
				unsigned int moy;
				for (unsigned int i = 0; i < nbThreads; i++) {
					moy = 0;
					for (unsigned int y = 0; y < pass; y++)
						moy += chrono[i][y];
					moy /= pass;
					output << (i+1) << "," << moy << std::endl;
				}
			}
			std::cout << "Outputed data in " << outputFile << std::endl;
			output.close();
		} catch (...) {
			err("An unexpected error occured while exporting statistics");
		}
	}

	~Chronos() {
		for (unsigned int i = 0; i < nbThreads; i++)
			delete chrono[i];
		delete chrono;
	}
};

int main(int argc, char **argv) {

	unsigned int threads = std::thread::hardware_concurrency(), batch = 30, length;
	bool fullStats = true;
	std::string outputFile = "output.csv", s;

	for (int i = 1; i < argc; i++) {

		s = std::string(argv[i]);
		length = s.length();

		if (s.substr(0,5) == "-help") {
			std::cout << "##### HELP MENU #####" << std::endl
				<< std::endl
				<< "This program is designed to work on Linux systems." << std::endl
				<< std::endl
				<< "Arguments:" << std::endl
				<< "    -threads=[VALUE]     to give a number of threads to work on. Default is the maximum amount of threads on this calculator." << std::endl
				<< "    -batch=[VALUE]       to give a number of batch to work on. Default is 30." << std::endl
				<< "    -outputFile=[NAME]   to give the name of the file to save data in. Notice that you can give a path and an extension, but data will be exported as csv. Default is output.csv in the same directory." << std::endl
				<< "    -fullStats=[BOOL]    to tell if you want to export all the chronos or if you only want averages. Default is true" << std::endl
				<< std::endl
				<< "This was created by Lucas TESSON (lucas.tesson@etu.univ-lyon1.fr) for the supervised project \"MandelBulb: fractales 3D\"" << std::endl;
			return EXIT_FAILURE;
		}
		else if (s.substr(0,9) == "-threads=" && length > 9) {
			try {
				threads = std::stoi(s.substr(9));
			} catch (std::invalid_argument const &e) {
				err("Bad input: std::invalid_argument thrown");
				return EXIT_FAILURE;
			} catch (std::out_of_range const &e) {
				err("Integer overflow: std::out_of_range thrown");
				return EXIT_FAILURE;
			}

		} else if (s.substr(0,7) == "-batch=" && length > 7) {
			try {
				batch = std::stoi(s.substr(7));
			} catch (std::invalid_argument const &e) {
				err("Bad input: std::invalid_argument thrown");
				return EXIT_FAILURE;
			} catch (std::out_of_range const &e) {
				err("Integer overflow: std::out_of_range thrown");
				return EXIT_FAILURE;
			}
		} else if (s.substr(0,12) == "-outputFile=" && length > 12) {
			outputFile = s.substr(12);
		} else if (s.substr(0,11) == "-fullStats=" && length > 11) {
			if (s.substr(11) == "true")
				fullStats = true;
			else if (s.substr(11) == "false")
				fullStats = false;
			else try {
				fullStats = (bool)std::stoi(s.substr(11));
			} catch (std::invalid_argument const &e) {
				err("Bad input: std::invalid_argument thrown");
				return EXIT_FAILURE;
			} catch (std::out_of_range const &e) {
				err("Integer overflow: std::out_of_range thrown");
				return EXIT_FAILURE;
			}
			if (!fullStats) std::cout << "Only average time will be exported" << std::endl;
		} else {
			err(std::string("Unrecognized parameter: ").append(argv[i]).c_str());
			return EXIT_FAILURE;
		}
	}

	Chronos *c = new Chronos(threads, batch, outputFile, fullStats);
	c->start();
	c->writeStats();
	delete c;

	return EXIT_SUCCESS;

}
